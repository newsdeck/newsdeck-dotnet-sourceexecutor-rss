#!/bin/bash
set -e

out=${1:0:40}
if [[ $out =~ ^v* ]]; then
    out=${out:1:40}
    echo $out
else
    echo "BAD_VERSION"
fi
