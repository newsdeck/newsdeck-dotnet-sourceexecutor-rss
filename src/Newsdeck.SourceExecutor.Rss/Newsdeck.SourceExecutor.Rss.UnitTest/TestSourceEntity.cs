using System;
using System.Collections.Generic;
using Newsdeck.Data.Entity;
using Newtonsoft.Json;

namespace Newsdeck.SourceExecutor.Rss.UnitTest
{
    public class TestSourceEntity : ISourceEntity
    {
        public TestSourceEntity(RssSourceJsonData data)
        {
            JsonData = JsonConvert.SerializeObject(data);
        }

        public long Identifier => 0;
        
        public long CreationUnixTime { get; } = DateTimeOffset.Now.ToUnixTimeMilliseconds();
        public long LastModificationUnixTime { get; } = DateTimeOffset.Now.ToUnixTimeMilliseconds();

        public string Type => "unitTest";

        public string JsonData { get; }

        public long NextCheckAfterUnixTime => 0;
        public long LastTimeChangedUnixTime => 0;
        public long? SourceIconIdentifier => null;
        public IEnumerable<IPublicationEntity> Publications => null;
        public IBlobEntity SourceIcon => null;
    }
}