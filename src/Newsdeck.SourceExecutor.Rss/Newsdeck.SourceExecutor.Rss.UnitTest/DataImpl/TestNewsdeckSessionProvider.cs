using Newsdeck.Data;

namespace Newsdeck.SourceExecutor.Rss.UnitTest.DataImpl
{
    internal class TestNewsdeckSessionProvider : INewsdeckSessionProvider
    {
        public IReadSession OpenReadSession()
        {
            return new TestReadSession();
        }

        public IWriteSession OpenWriteSession()
        {
            throw new System.NotImplementedException();
        }
    }
}