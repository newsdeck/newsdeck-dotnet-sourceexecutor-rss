using System.Threading;
using System.Threading.Tasks;
using Newsdeck.Data;
using Newsdeck.Data.Entity;
using Newsdeck.Data.Repository;

namespace Newsdeck.SourceExecutor.Rss.UnitTest.DataImpl
{
    public class TestPublicationReadRepository : IPublicationReadRepository
    {
        private readonly TestPublicationDataResult _pool = new ();
        
        public TestPublicationReadRepository(IReadSession session)
        {
            Session = session;
        }

        public Task<IPublicationEntity> GetWithIdAsync(long id, CancellationToken cancellationToken)
        {
            cancellationToken.ThrowIfCancellationRequested();
            return Task.FromResult(_pool.Get(id));
        }

        public IDataResult<IPublicationEntity, long> Get()
        {
            return _pool;
        }

        public IReadSession Session { get; set; }
    }
}