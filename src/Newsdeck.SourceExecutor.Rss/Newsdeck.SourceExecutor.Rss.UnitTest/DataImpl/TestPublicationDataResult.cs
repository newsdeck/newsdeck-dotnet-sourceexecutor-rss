using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Newsdeck.Data;
using Newsdeck.Data.Entity;

namespace Newsdeck.SourceExecutor.Rss.UnitTest.DataImpl
{
    public class TestPublicationDataResult : IDataResult<IPublicationEntity, long>
    {
        private readonly Dictionary<long, IPublicationEntity> _dic = new();
        
        public Task<List<IPublicationEntity>> AsListAsync(CancellationToken cancellationToken)
        {
            var r = _dic.ToList().Select(o => o.Value);
            return Task.FromResult(r.ToList());
        }

        public IPublicationEntity Get(long id)
        {
            return _dic[id];
        }

        public void AddFilter(IFilter filter)
        {
            // do nothing in this unit test;
        }

        public void SetLimit(int? limit)
        {
            // do nothing in this unit test;
        }

        public void AddOrder(IOrder order)
        {
            // do nothing in this unit test;
        }
    }
}