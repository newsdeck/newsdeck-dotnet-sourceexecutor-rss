using System.Threading.Tasks;
using Newsdeck.Data;
using Newsdeck.Data.Repository;

namespace Newsdeck.SourceExecutor.Rss.UnitTest.DataImpl
{
    public class TestReadSession : IReadSession
    {
        public ValueTask DisposeAsync()
        {
            return ValueTask.CompletedTask;
        }

        public IBlobReadRepository GetBlobReadRepository()
        {
            throw new System.NotImplementedException();
        }

        public IPublicationReadRepository GetPublicationReadRepository()
        {
            return new TestPublicationReadRepository(this);
        }

        public ITagReadRepository GetTagReadRepository()
        {
            throw new System.NotImplementedException();
        }

        public ISourceReadRepository GetSourceReadRepository()
        {
            throw new System.NotImplementedException();
        }
    }
}