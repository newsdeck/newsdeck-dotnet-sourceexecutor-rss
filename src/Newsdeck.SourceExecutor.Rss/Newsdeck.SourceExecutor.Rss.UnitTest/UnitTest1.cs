using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;

namespace Newsdeck.SourceExecutor.Rss.UnitTest
{
    public class Tests
    {
        [Theory]
        [TestCase("https://codeburst.io/feed")]
        [TestCase("https://dev.to/feed")]
        [TestCase("https://medium.com/feed/angular-in-depth")]
        [TestCase("https://realpython.com/atom.xml")]
        [TestCase("https://blogs.apache.org/foundation/feed/entries/atom")]
        [TestCase("https://www.phpclasses.org/browse/latest/latest.xml")]
        [TestCase("https://www.phpclasses.org/browse/latest/latest.xml")]
        [TestCase("https://blog.golang.org/feed.atom")]
        public async Task TestSource(string url)
        {
            var cancellationToken = CancellationToken.None;
            var instance = Instance.Get();
            var executor = instance.GetRssExecutor();

            var data = new RssSourceJsonData
            {
                RssFeedUrl = url
            };
            var result = await executor.LoadArticlesAsync(new TestSourceEntity(data), cancellationToken);
            Assert.NotNull(result.ToCreate);
            foreach (var createPublicationModel in result.ToCreate)
            {
                Assert.NotNull(createPublicationModel.OriginalGuid);
            }
        }

        [Theory]
        [TestCase("https://codeburst.io/feed", 90)]
        [TestCase("https://dev.to/feed", 90)]
        [TestCase("https://localhost:1000/notExistingEndpoint", 0)]
        [TestCase("h---", 0)]
        public async Task TestCheckHandlingCapabilitiesForInputAsync(string url, int expectedResult)
        {
            var cancellationToken = CancellationToken.None;
            var instance = Instance.Get();
            var executor = instance.GetRssExecutor();

            var capabilitiesResult = await executor.CheckHandlingCapabilitiesForInputAsync(url, cancellationToken);
            Assert.AreEqual(expectedResult, capabilitiesResult);
        }
    }
}