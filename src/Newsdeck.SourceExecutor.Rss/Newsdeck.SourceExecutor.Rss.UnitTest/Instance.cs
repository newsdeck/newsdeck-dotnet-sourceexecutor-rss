using System;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newsdeck.Data;
using Newsdeck.Shared;
using Newsdeck.SourceExecutor.Rss.UnitTest.DataImpl;
using Newsdeck.SourceExecutor.Rss.UnitTest.Log;

namespace Newsdeck.SourceExecutor.Rss.UnitTest
{
    public class Instance : IAsyncDisposable
    {

        public static Instance Get()
        {
            var instance = new Instance();
            instance.Initialize();
            return instance;
        }

        private readonly ServiceProvider _serviceProvider;
        private readonly Type _executorType;

        private Instance()
        {
            var serviceCollection = new ServiceCollection();
            serviceCollection.AddSingleton<INewsdeckSessionProvider, TestNewsdeckSessionProvider>();
            serviceCollection.AddLogging(conf =>
            {
                conf.SetMinimumLevel(LogLevel.Trace);
                conf.AddProvider(new TestLogProvider());
            });
            var loader = new Loader();
            loader.Register(serviceCollection);
            _executorType = loader.GetExecutorType();
            serviceCollection.AddTransient(_executorType);
            _serviceProvider = serviceCollection.BuildServiceProvider();
        }

        public ISourceExecutor GetRssExecutor()
        {
            var res = _serviceProvider.GetService(_executorType) as ISourceExecutor;
            if (res == null)
            {
                throw new ApplicationException("Can not retrieve RssExecutor");
            }
            return res;
        }

        public T GetInstance<T>()
        {
            return _serviceProvider.GetService<T>();
        }

        private static bool _wasInit = false;
        public void Initialize()
        {
            if (_wasInit)
            {
                return;
            }
            _wasInit = true;
            var logger = _serviceProvider.GetService<ILogger<Instance>>();
            logger?.LogInformation("Init OK.");
        }

        public ValueTask DisposeAsync()
        {
            _serviceProvider?.Dispose();
            return new ValueTask(Task.CompletedTask);
        }
    }
}