using Newsdeck.Data.Model;

namespace Newsdeck.SourceExecutor.Rss
{
    internal class DefaultPublicationData : ICreatePublicationModel, IEditPublicationModel
    {
        public string JsonData { get; set; }
        public long SourceIdentifier { get; set; }
        public long? PublicationCoverIdentifier { get; set; }
        public string OriginalGuid { get; set; }
        public long PublicationDateUnixTime { get; set; }
    }
}