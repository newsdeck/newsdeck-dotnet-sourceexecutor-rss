using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Newsdeck.Data;
using Newsdeck.Data.Entity;
using Newsdeck.Data.Filter;
using Newsdeck.Data.Model;
using Newsdeck.Shared;
using Newsdeck.SourceExecutor.Rss.RssParser;
using Newtonsoft.Json;

namespace Newsdeck.SourceExecutor.Rss
{
    internal class RssSourceExecutor : ISourceExecutor
    {

        private readonly INewsdeckSessionProvider _newsdeckSessionProvider;

        public RssSourceExecutor(INewsdeckSessionProvider newsdeckSessionProvider)
        {
            _newsdeckSessionProvider = newsdeckSessionProvider;
        }

        public async Task<IRssParsingResult> LoadSourceAsync(string url, CancellationToken cancellationToken)
        {
            var rssParser = await IRssParser.FromUrlAsync(url, cancellationToken);
            var parsingResult = rssParser.Parse();
            return parsingResult;
        }

        private async Task<RssSourceExecutorResult> LoadArticles(ISourceEntity entity,
            RssSourceJsonData data,
            CancellationToken cancellationToken)
        {
            var toCreate = new List<ICreatePublicationModel>();
            var toEdit = new Dictionary<long, IEditPublicationModel>();
            // TODO handle deleted items 
            var parsingResult = await LoadSourceAsync(data.RssFeedUrl, cancellationToken);
            var existing = await LoadExistingPublications(entity, parsingResult, cancellationToken);
            if (parsingResult.IsValid)
            {
                foreach (var parsingResultItem in parsingResult.Items)
                {
                    var rssData = new RssPublicationJsonData
                    {
                        Content = parsingResultItem.Content,
                        Link = parsingResultItem.Link,
                        ShortDescription = parsingResultItem.ShortDescription,
                        PublishedOn = parsingResultItem.PublishedOn
                    };
                    var publicationData = new DefaultPublicationData
                    {
                        OriginalGuid = parsingResultItem.OriginGuid,
                        SourceIdentifier = entity.Identifier,
                        PublicationCoverIdentifier = null, // TODO
                        JsonData = JsonConvert.SerializeObject(rssData),
                        PublicationDateUnixTime = parsingResultItem.PublishedOn.ToUnixTimeMilliseconds()
                    };
                    var r = existing.FirstOrDefault(o =>
                        o.OriginalGuid.Equals(parsingResultItem.OriginGuid,
                            StringComparison.InvariantCultureIgnoreCase));
                    if (r != null)
                    {
                        if (!publicationData.JsonData.Equals(r.JsonData))
                        {
                            toEdit[r.Identifier] = publicationData;
                        }
                    }
                    else
                    {
                        toCreate.Add(publicationData);
                    }
                }
            }
            return new RssSourceExecutorResult(toCreate, toEdit, new List<long>());
        }

        private async Task<List<IPublicationEntity>> LoadExistingPublications(ISourceEntity entity, 
            IRssParsingResult parsingResult,
            CancellationToken cancellationToken)
        {
            var items = parsingResult.Items.ToList();
            await using (var readSession = _newsdeckSessionProvider.OpenReadSession())
            {
                var repository = readSession.GetPublicationReadRepository();
                var query = repository.Get();
                var l = new List<object>();
                l.AddRange(items.Select(o => o.OriginGuid.ToLowerInvariant()));
                query.FilterBy(new InFilter<IPublicationEntity>(o => o.OriginalGuid, l))
                    .FilterBy(new EqualFilter<IPublicationEntity>(o => o.SourceIdentifier, entity.Identifier))
                    .Limit(items.Count);
                return await query.AsListAsync(cancellationToken);
            }
        }
        
        public async Task<ISourceExecutorResult> LoadArticlesAsync(ISourceEntity entity, CancellationToken cancellationToken)
        {
            var data = string.IsNullOrWhiteSpace(entity?.JsonData)
                ? new RssSourceJsonData()
                : JsonConvert.DeserializeObject<RssSourceJsonData>(entity.JsonData);
            if (data == null)
            {
                throw new RssSourceInvalidException(entity);
            }
            var res = await LoadArticles(entity, data, cancellationToken);
            return res;
        }

        public async Task<int> CheckHandlingCapabilitiesForInputAsync(string inputUrl, CancellationToken cancellationToken)
        {
            var result = Uri.TryCreate(inputUrl, UriKind.Absolute, out var uriResult) 
                          && (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps);

            if (result == false)
            {
                return 0; // not a valid url
            }
            var r = await LoadSourceAsync(uriResult.ToString(), cancellationToken);
            if (r?.IsValid ?? false)
            {
                return 90;
            }
            return 0;
        }
    }
}