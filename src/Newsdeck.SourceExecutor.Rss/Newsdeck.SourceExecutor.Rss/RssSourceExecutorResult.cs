using System.Collections.Generic;
using Newsdeck.Data.Model;
using Newsdeck.Shared;

namespace Newsdeck.SourceExecutor.Rss
{
    internal class RssSourceExecutorResult : ISourceExecutorResult
    {
        public IEnumerable<ICreatePublicationModel> ToCreate { get; }
        public IDictionary<long, IEditPublicationModel> ToEdit { get; }
        public IEnumerable<long> ToDelete { get; }

        internal RssSourceExecutorResult(IEnumerable<ICreatePublicationModel> toCreate, 
            IDictionary<long, IEditPublicationModel> toEdit, 
            IEnumerable<long> toDelete)
        {
            ToCreate = toCreate;
            ToEdit = toEdit;
            ToDelete = toDelete;
        }
    }
}