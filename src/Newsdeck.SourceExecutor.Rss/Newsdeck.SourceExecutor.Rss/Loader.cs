using System;
using Microsoft.Extensions.DependencyInjection;
using Newsdeck.Shared;

namespace Newsdeck.SourceExecutor.Rss
{
    /// <summary>
    /// Assembly Loader class target
    /// for package Newsdeck.SourceExecutor.Rss
    /// </summary>
    public class Loader : ILoader
    {
        /// <inheritdoc/>
        public void Register(IServiceCollection serviceCollection)
        {
            serviceCollection.AddTransient<RssSourceExecutor>();
        }

        /// <inheritdoc/>
        public Type GetExecutorType()
        {
            return typeof(RssSourceExecutor);
        }
    }
}