namespace Newsdeck.SourceExecutor.Rss
{
    /// <summary>
    /// Default RssSourceJsonData
    /// </summary>
    public class RssSourceJsonData
    {
        /// <summary>
        /// Store the rss access url
        /// </summary>
        public string RssFeedUrl { get; set; }
    }
}