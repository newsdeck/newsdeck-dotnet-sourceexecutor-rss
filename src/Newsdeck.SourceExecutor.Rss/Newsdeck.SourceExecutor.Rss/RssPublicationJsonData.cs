using System;

namespace Newsdeck.SourceExecutor.Rss
{
    internal class RssPublicationJsonData
    {
        public string Content { get; set; }
        public string Link { get; set; }
        public string ShortDescription { get; set; }
        public DateTimeOffset PublishedOn { get; set; }
    }
}