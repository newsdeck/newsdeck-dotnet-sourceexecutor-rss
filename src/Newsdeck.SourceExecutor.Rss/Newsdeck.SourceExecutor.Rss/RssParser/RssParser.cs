﻿using System;
using System.Collections.Generic;
using System.Linq;
using CodeHollow.FeedReader;

namespace Newsdeck.SourceExecutor.Rss.RssParser
{
    internal class RssParser : IRssParser
    {
        private readonly Feed _syndicationFeed;
        private readonly IEnumerable<string> _errors;

        internal RssParser(Feed syndicationFeed)
        {
            _syndicationFeed = syndicationFeed;
        }
        
        internal RssParser(IEnumerable<string> errors)
        {
            _errors = errors;
        }

        public IRssParsingResult Parse()
        {
            return IsValid()
                ? new RssParsingResult
                {
                    Items = ParseItems(),
                    BlogUrl = _syndicationFeed?.Link,
                    IsValid = true,
                    BlogName = _syndicationFeed?.Title,
                    BlogDescription = _syndicationFeed?.Description
                }
                : new RssParsingResult
                {
                    IsValid = false
                };
        }

        public bool IsValid()
        {
            return _syndicationFeed != null && !(_errors?.Any() ?? false);
        }

        private IEnumerable<IRssItem> ParseItems()
        {
            return _syndicationFeed.Items
                .Select(TreatSyndicationItem)
                .ToList();
        }

        private static IRssItem TreatSyndicationItem(FeedItem e)
        {
            var html = e.Content ?? e.Description;
            var articleLink = e.Link;
            return new RssItem
            {
                Content = html,
                Categories = (e.Categories?.Any() ?? false) ? e.Categories.ToList() : null,
                Link = articleLink,
                OriginGuid = string.IsNullOrWhiteSpace(e.Id) ? e.Link : e.Id,
                PublishedOn = e.PublishingDate.HasValue ? new DateTimeOffset(e.PublishingDate.Value) : DateTimeOffset.Now,
                ShortDescription = e.Title
            };
        }
    }
}