using System;
using System.Collections.Generic;

namespace Newsdeck.SourceExecutor.Rss.RssParser
{
    internal interface IRssItem
    {
        string Content { get; }
        string OriginGuid { get; }
        string Link { get; }
        string ShortDescription { get; }
        DateTimeOffset PublishedOn { get; }
        IEnumerable<string> Categories { get; }
    }
    
    internal class RssItem : IRssItem
    {
        public string Content { get; set; }
        public string OriginGuid { get; set; }
        public string Link { get; set; }
        public string ShortDescription { get; set; }
        public DateTimeOffset PublishedOn { get; set; }
        public IEnumerable<string> Categories { get; set; }
    }
}