using System.Collections.Generic;

namespace Newsdeck.SourceExecutor.Rss.RssParser
{
    internal interface IRssParsingResult
    {
        string BlogName { get; }
        string BlogDescription { get; }
        public string BlogUrl { get; }
        IEnumerable<IRssItem> Items { get; }
        bool IsValid { get; }
    }
    
    internal class RssParsingResult : IRssParsingResult
    {
        public string BlogName { get; set; }
        public string BlogDescription { get; set; }
        public string BlogUrl { get; set; }
        public IEnumerable<IRssItem> Items { get; set; }
        public bool IsValid { get; set; }
    }
}