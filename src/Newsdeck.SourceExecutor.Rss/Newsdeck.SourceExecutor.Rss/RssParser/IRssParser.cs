using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using CodeHollow.FeedReader;
using Newsdeck.Shared.HttpDownloader;

namespace Newsdeck.SourceExecutor.Rss.RssParser
{
    internal interface IRssParser
    {
        public static async Task<IRssParser> FromUrlAsync(string url, CancellationToken cancellationToken)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(url))
                {
                    throw new ArgumentNullException(nameof(url));
                }

                var downloader = new AsyncHttpDownloader(TimeSpan.FromSeconds(10));
                var content = await downloader.GetAsync(url, cancellationToken);
                if (content.IsInError)
                {
                    return new RssParser(new List<string> { content.Error.ToString() });
                }
                var contentString = await content.GetResultContentAsync();
                var f = FeedReader.ReadFromString(contentString);
                return new RssParser(f);
            }
            catch (XmlException e)
            {
                return new RssParser(new List<string> { e.ToString() });
            }
        }

        public static IRssParser FromContent(string content)
        {
            if (string.IsNullOrWhiteSpace(content))
            {
                throw new ArgumentNullException(nameof(content));
            }
            var f = FeedReader.ReadFromString(content);
            return new RssParser(f);
        }

        IRssParsingResult Parse();
    }
}