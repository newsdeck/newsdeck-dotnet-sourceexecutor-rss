using System;
using Newsdeck.Data.Entity;

namespace Newsdeck.SourceExecutor.Rss
{
    
    /// <summary>
    /// RssSourceInvalidException is an exception thrown when
    /// a given executing source is not invalid and can not be treated
    /// </summary>
    [Serializable]
    public class RssSourceInvalidException : Exception
    {
        private readonly ISourceEntity _sourceEntity;

        /// <summary>
        /// Targeted ISourceEntity
        /// </summary>
        public ISourceEntity RssSource => _sourceEntity;
        
        internal RssSourceInvalidException(ISourceEntity sourceEntity)
            : base($"The source is invalid, check RssSourceInvalidException.RssSource to check the source")
        {
            _sourceEntity = sourceEntity;
        }
    }
}